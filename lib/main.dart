import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

import 'UI/register_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Bliss Touch',
      home: MyHomePage(title: 'Bliss Touch'),
      darkTheme: ThemeData.dark().copyWith(
          primaryColor: Colors.cyan[900],
          primaryColorLight: Colors.cyan[500],
          toggleableActiveColor: Colors.deepPurpleAccent[200],
          accentColor: Colors.deepPurpleAccent[200],
          bottomAppBarColor: Colors.cyan[800],
          cardColor: Colors.cyan[800],
          secondaryHeaderColor: Colors.cyan[700],
          textSelectionHandleColor: Colors.deepPurpleAccent[400],
          backgroundColor: Colors.cyan[700],
          dialogBackgroundColor: Colors.cyan[800],
          buttonTheme: ButtonThemeData(
              buttonColor: Colors.cyan[600],
              shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(18.0),
              )),
          textTheme: GoogleFonts.quicksandTextTheme(Theme.of(context).textTheme)
              .copyWith(
                  button: GoogleFonts.quicksand(
                      color: Colors.cyan[100], fontWeight: FontWeight.w700))),
      theme: ThemeData(
          primarySwatch: Colors.cyan,
          accentColor: Colors.deepPurpleAccent,
          buttonTheme: ButtonThemeData(
              buttonColor: Colors.cyan[300],
              shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(18.0),
              )),
          textTheme: GoogleFonts.quicksandTextTheme(Theme.of(context).textTheme)
              .copyWith(
                  button: GoogleFonts.quicksand(
                      color: Colors.cyan[900], fontWeight: FontWeight.w700))),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  FirebaseUser user;

  @override
  Widget build(BuildContext context) {
    final Brightness brightnessValue =
        MediaQuery.of(context).platformBrightness;
    bool isDark = brightnessValue == Brightness.dark;

    return Scaffold(
      body: Stack(children: <Widget>[
        Positioned.fill(
          child: Opacity(
            opacity: 0.33,
            child: Container(
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("images/home_background.jpg"),
                      fit: BoxFit.cover)),
            ),
          ),
        ),
        Positioned(
          top: MediaQuery.of(context).size.width * -0.81,
          left: MediaQuery.of(context).size.width * -0.25,
          child: Opacity(
            opacity: 0.66,
            child: Container(
              width: MediaQuery.of(context).size.width * 1.5,
              height: MediaQuery.of(context).size.width * 1.5,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: new DecorationImage(
                    fit: BoxFit.fill,
                    image: new AssetImage(
                        "images/home_image.jpg"),
                    colorFilter: (isDark)
                        ? ColorFilter.mode(Colors.cyan[600], BlendMode.colorBurn)
                        : ColorFilter.mode(
                            Colors.cyan[300], BlendMode.colorBurn)),
              ),
            ),
          ),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            SizedBox(
              height: MediaQuery.of(context).size.width * 0.5,
//              child: Container(
//                color: Colors.red,
//              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.5,
              child: Stack(
                children: <Widget>[
                  Center(
                    child: SvgPicture.asset(
                      'images/logo_bliss_touch.svg',
                      semanticsLabel: 'Logo BlissTouch',
                      color: (isDark) ? Colors.cyan[600] : Colors.cyan[300],
                      height: MediaQuery.of(context).size.height * 0.22,
                      //width: 128,
                    ),
                  ),
                  Column(
                    children: [
                      Center(
                        child: Text(
                          'Bliss Touch',
                          style: GoogleFonts.molle(
                            textStyle: Theme.of(context).textTheme.headline5,
                            color:
                                (isDark) ? Colors.cyan[600] : Colors.cyan[300],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.11,
                      )
                    ],
                    mainAxisAlignment: MainAxisAlignment.end,
                  )
                ],
              ),
            ),
            Container(
              child: SizedBox(
                width: MediaQuery.of(context).size.width * 0.77,
                height: MediaQuery.of(context).size.height * 0.07,
                child: RaisedButton(
                  child: Text('Iniciar sesión'.toUpperCase(),
                      style: GoogleFonts.quicksand(
                          textStyle: Theme.of(context).textTheme.button)),
                  onPressed: () => _pushPage(context, RegisterPage()),
                ),
              ),
              padding: const EdgeInsets.all(8),
              alignment: Alignment.center,
            ),
            Container(
              child: SizedBox(
                width: MediaQuery.of(context).size.width * 0.77,
                height: MediaQuery.of(context).size.height * 0.07,
                child: OutlineButton(
                  child: Text('Registrarse'.toUpperCase(),
                      style: GoogleFonts.quicksand(
                          textStyle: Theme.of(context).textTheme.button)),
                  onPressed: () => _pushPage(context, RegisterPage()),
                ),
              ),
              padding: const EdgeInsets.all(8),
              alignment: Alignment.center,
            ),
          ],
        ),
      ]),
    );
  }

  void _pushPage(BuildContext context, Widget page) {
    Navigator.of(context).push(
      MaterialPageRoute<void>(builder: (_) => page),
    );
  }
}

class Record {
  final String name;
  final int votes;
  final DocumentReference reference;

  Record.fromMap(Map<String, dynamic> map, {this.reference})
      : assert(map['name'] != null),
        assert(map['votes'] != null),
        name = map['name'],
        votes = map['votes'];

  Record.fromSnapshot(DocumentSnapshot snapshot)
      : this.fromMap(snapshot.data, reference: snapshot.reference);

  @override
  String toString() => "Record<$name:$votes>";
}
